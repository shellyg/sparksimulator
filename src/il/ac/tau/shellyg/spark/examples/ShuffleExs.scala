package il.ac.tau.shellyg.spark.examples

// scalastyle:off println
import il.ac.tau.shellyg.spark._

/** Computes an approximation to pi */
object ShuffleExs {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Spark Pi")
    val sc = new SparkContext(conf)
    val a = sc.parallelize(1 to 10, 3)

    val cart = a.cartesian(a)

    println(cart.collect())
//    val shuffledNonCart = a.shuffle(new HashPartitioner(2)) // Will result in exception
    val shuffledCart = cart.shuffle(new HashPartitioner(2))
    println(shuffledCart.collect())


    println("Example 2")
    val b = a.map((_, 1))
    println(b.partitionBy(new HashPartitioner(2)).collect()) // Partitions by Even/Odd ?! Yes. Note that it is a synthetic transform, not necessarily equal to coalesce
    sc.stop()
  }
}

// scalastyle:on println
