package il.ac.tau.shellyg.spark.examples.badcode

// scalastyle:off println
import il.ac.tau.shellyg.spark._

import scala.reflect.ClassTag

/** Computes an approximation to pi */
object ClosureErrorEx {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Closure error")
    val sc = new SparkContext(conf)

    var counter = 0
    val a = sc.parallelize(1 to 10, 5)
    def closure(x : Int) : Int = { // A function using an external variable counter, not part of closure
      counter += x
      counter
    }

    var result1 = a.map(closure)
    println(result1.glom.collect) // Simulation yields a reasonable result

    counter = 0 // Nullifying the counter
    var result2 = a.mapMess(closure) // TODO: Make "-mess"-y transforms generically (better name: entropyTransform?)
    println(result2.glom.collect) // Randomizing the order in which we're working on the partitions causes a mess
    println("Comparing the two RDDs: "+ result1.compare(result2))

    // This is unsound method...
    def safetyCheckerMap(obj : RDD[Int], closure : Int => Int, maxNumOfAttmpets : Int) = {
      var safe = true
      var numOfAttempts = 0
      val result1 = obj.map(closure)
      do {
        counter = 0
        var result2 = obj.mapMess(closure) // TODO: Add a "resetting" method? So between partition, restarts to how the partition sees it, if it is another host
        safe = result1.compare(result2)
        if (!safe) {
          println("The operation is not safe")
          false
        } else {
          numOfAttempts += 1
        }
      } while (safe && numOfAttempts < maxNumOfAttmpets);
      safe
    }
    println("is safe transform? " + safetyCheckerMap(a, closure, 100))

    // Now we try to analyze the closure with reflection just to see abilities
    def closureAnalyzer(c : Int => Int) = {
      // Reify usage..
      import scala.reflect.runtime.universe._
      var tree = reify(c).tree
      println("Closure's tree: " + showRaw(tree))
      println("Closure's class tree: " + showRaw(reify(c.getClass).tree))
      println("Closure's enclosing class tree: " + showRaw(reify(c.getClass.getEnclosingClass).tree))

      // Macros usage (Compile time!)
      // import scala.language.experimental.macros
      // def getInfo(c : => Any) : Unit = macro FindFreeVars.findMacro
      // See https://github.com/ejoebstl/ScalaMacros/blob/master/macros/src/main/scala/FindFreeVars.scala
      // And: http://stackoverflow.com/questions/24480926/how-can-i-use-scalas-runtime-reflection-to-inspect-a-passed-anonymous-function

      println("Closure class: " + c.getClass)
      var fields = c.getClass.getFields
      for ( i <- 0 to fields.length-1) {
        println("Field #" + i + ": " + fields.apply(i))
      }

      var declaredMethods = c.getClass.getDeclaredMethods
      for ( i <- 0 to declaredMethods.length-1) {
        println("Declared Method #" + i + ": " + declaredMethods.apply(i))
      }

      var enclosingClass = c.getClass.getEnclosingClass
      println("Enclosing class: " + enclosingClass)

      var enclosingDeclaredMethods = enclosingClass.getDeclaredMethods
      for ( i <- 0 to enclosingDeclaredMethods.length-1) {
        var method = enclosingDeclaredMethods.apply(i)
        println("Enclosing's Declared Method #" + i + ": " + enclosingDeclaredMethods.apply(i))
      }

    }

    closureAnalyzer(closure)

    sc.stop()
  }
}

// scalastyle:on println
