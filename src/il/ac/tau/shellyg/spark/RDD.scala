package il.ac.tau.shellyg.spark

import scala.collection.immutable.ListMap
import scala.collection.mutable
import scala.collection.mutable.Buffer
import scala.reflect.ClassTag
import scala.util.Random

/**
  * Created by shelly on 20/01/16.
  */
class RDD[T : ClassTag] (val partitions: Map[RddTypes.PID, Buffer[T]], val numSlices: Int) {
  val SHOULD_DEBUG = false

  def println(x : Any) = { Utils.println(x) }

  private val typeTupleCardinality =  if (!implicitly[ClassTag[T]].runtimeClass.isPrimitive) {
    // Handle primitive defaults
    val name = implicitly[ClassTag[T]].runtimeClass.getName
    if (SHOULD_DEBUG) println(name)
    if (name.contains("scala.Tuple")) {
      if (SHOULD_DEBUG) println("The integer: " + name.substring("scala.Tuple".length))
      Integer.decode(name.substring("scala.Tuple".length))
    } else { 1 }
  } else { 1 }

  val rid = RDDRepo.getNextRid()

  def reduce(f: (T, T) => T) = {
    /*
      First, reduce all partitions.
      Then, reduce on all partitions' results
     */
    var intermediateValue : Any = None


    if (implicitly[ClassTag[T]].runtimeClass.isPrimitive) { // Handle primitive defaults
      if (SHOULD_DEBUG) println(implicitly[ClassTag[T]].runtimeClass.getName)
      intermediateValue = implicitly[ClassTag[T]].runtimeClass.getName match {
        case "int" => 0
        case "long" => 0
        case "byte" => 0
        case "short" => 0
        case "char" => 0
        case "boolean" => false
        case "float" => 0.0
        case "double" => 0.0
      }
    } else {
      val realClass = implicitly[ClassTag[T]].runtimeClass.getName
      println(implicitly[ClassTag[T]].runtimeClass.getName)
      if (realClass.equals("java.lang.Object")) {
        intermediateValue = 0
      } else {
        intermediateValue = implicitly[ClassTag[T]].runtimeClass.newInstance().asInstanceOf[T] // What do I do about primitive ints ?
      }
    }

    if (SHOULD_DEBUG)   println("Partitions = " + partitions)
    for (elem <- partitions) {
      if (SHOULD_DEBUG) println("Working on partition: " + elem)
      val partitionBuffer = elem._2.asInstanceOf[Buffer[T]]
      if (!partitionBuffer.isEmpty) {
        val partitionReduceElem = partitionBuffer.reduce(f)
        if (SHOULD_DEBUG) println("Reduced partition " + elem._1 + " has value: " + partitionReduceElem + ", intermediate value is " + intermediateValue)
        intermediateValue = f(intermediateValue.asInstanceOf[T], partitionReduceElem)
        if (SHOULD_DEBUG) println(partitionReduceElem)
        if (SHOULD_DEBUG) println(intermediateValue)
      }
    }

    intermediateValue.asInstanceOf[T]

  }

  def map[U: ClassTag](f: T => U) : RDD[U] = {
    val newPartitions = for ((pid, partition) <- partitions) yield (pid, partition.map(f))
    RDDRepo.addNewWithEdge(new RDD[U](newPartitions, numSlices), rid, "map")
  }

  def mapToPair[K: ClassTag, V: ClassTag](f: T => (K,V)) : PartitionedRDD[K,V] = {
    val newPartitions = for ((pid, partition) <- partitions) yield (pid, partition.map(f))

    // Map must return a PartitionedRDD if U is a tuple type
    val newRdd = new RDD[(K,V)](newPartitions, numSlices)
    val newPartitionedRDD = new PartitionedRDD[K,V](newRdd, None)
    RDDRepo.addNewWithEdgePartitioned(newPartitionedRDD, rid, "mapToPair", false /* Always an internal call - use private?*/)
  }

  def asPartitionedRDD[K: ClassTag, V: ClassTag]() : PartitionedRDD[K, V] = {
    // Map to self
    def f(x : T) : (K, V) = {
      x.asInstanceOf[(K, V)]
    }

    this.mapToPair[K, V](f)
  }

  def filter(f: (T) => Boolean): RDD[T] = {
    val newPartitions = for ((pid, partition) <- partitions) yield (pid, partition.filter(f))
    RDDRepo.addNewWithEdge(new RDD[T](newPartitions, numSlices), rid, "filter")
  }

  def flatMap[U: ClassTag](f: (T) => TraversableOnce[U]) : RDD[U] = {
    val newPartitions = for ((pid, partition) <- partitions) yield {println("flatMap works on pid: " + pid)
      (pid, partition.flatMap(f))}
    RDDRepo.addNewWithEdge(new RDD[U](newPartitions, numSlices), rid, "flatMap")
  }

  def cartesian[U: ClassTag](other : RDD[U]) : PartitionedRDD[T,U] = {
    var newPartitions = mutable.Map[RddTypes.PID, Buffer[(T,U)]]()

    if (SHOULD_DEBUG) println(partitions.keys.toList)
    // Num of partitions is the product
    val newNumSlices = partitions.keys.size * other.partitions.keys.size

    for (p1 <- 0 to partitions.keys.size-1 ; p2 <- 0 to other.partitions.keys.size-1) {
      val p = p1 * other.partitions.keys.size + p2
      if (SHOULD_DEBUG) println("Working on partition " + p)
      val partitionPreparation = for {elem1 <- partitions.get(partitions.keys.toList.apply(p1));
                                      elem2 <- other.partitions.get(other.partitions.keys.toList.apply(p2))}
                                        yield (elem1, elem2)

      val partition = for {e1 <- partitionPreparation.get._1
                          e2 <- partitionPreparation.get._2}
                            yield (e1,e2)


      newPartitions += ((p.toInt, partition))
    }

    val newRDD = new RDD[(T,U)](newPartitions.toMap, newNumSlices)
    RDDRepo.addNewWithEdgePartitioned(new PartitionedRDD[T,U](newRDD, None), rid, "cartesian")
  }

  def union(others: RDD[T]*) : RDD[T] = {
      println("Called union with a sequence")
      RDDRepo.addNewWithEdge(others.foldLeft(this)(_ ++ _), rid, "union")
  }

  def union(other: RDD[T], isFromUser : Boolean = true) : RDD[T] = {
    if (SHOULD_DEBUG) println("This partitions: " + partitions.keys.size + ", other partitions: " + other.partitions.keys.size)
      var newPartitions = mutable.Map[RddTypes.PID, Buffer[T]]()

      val newNumSlices = partitions.keys.size + other.partitions.keys.size
      for (p <- 0 to partitions.keys.size - 1) {
        newPartitions += ((p.toInt, partitions.get(p).get))
      }
      for (p <- partitions.keys.size to newNumSlices - 1) {
        val otherPid = (p - partitions.keys.size)
        if (SHOULD_DEBUG) {
          println("Getting from other the PID " + otherPid)
          println(other.partitions)
        }
        newPartitions += ((p.toInt, other.partitions.get(otherPid).get))
      }

      RDDRepo.addNewWithEdge(new RDD[T](newPartitions.toMap, newNumSlices), rid, "union", isFromUser)
  }

  // Currently, preserves partitioning
  def mapPartitions[U: ClassTag](f: Iterator[T] => Iterator[U], isFromUser : Boolean = true) : RDD[U] = {
    var newPartitions = mutable.Map[RddTypes.PID, Buffer[U]]()
    for (p <- partitions.keys) {
      val newPartition = f(partitions.get(p).get.toIterator).toBuffer
      newPartitions += ((p.toInt, newPartition))
    }

    RDDRepo.addNewWithEdge(new RDD[U](newPartitions.toMap, numSlices), rid, "mapPartitions", isFromUser)
  }

  def ++(other: RDD[T]) = {this union other}

  def repartition(numPartitions : Int) = { // TODO: Coalesce - when decreasing number of partitions
      // Applicable to any kind of RDD.

  }

  def groupBy[K: ClassTag](f: T => K, partitioner: Partitioner): PartitionedRDD[K, Iterable[T]] = {
    // Go over all partitions, get the key, and add to the new RDD
    var groupedPartitions = mutable.Map[RddTypes.PID, Buffer[(K, T)]]() // Will turn to (K, Iterable[T]) later
    for (p <- partitions.keys) {  // Go over all partitions
      for (t <- partitions.get(p).get) { // Go over all elements in partition
        val key = f(t)
        val partitionOfKey = partitioner.getPartition(key)
        if (groupedPartitions.contains(partitionOfKey)) {
          groupedPartitions.get(partitionOfKey).get += ((key, t))
        } else {
          val buf = mutable.Buffer((key, t))
          groupedPartitions += ((partitionOfKey, buf))
        }
      }
    }

    // Now convert to Buffer[(K, Iterable[T])]
    var newGroupedPartitions = mutable.Map[RddTypes.PID, Buffer[(K, Iterable[T])]]()
    for (p <- groupedPartitions.keys) {
      val buf = groupedPartitions.get(p).get
      val newBuf = buf.toIterable.groupBy(_._1).mapValues(_.map(_._2)).toBuffer
      newGroupedPartitions += ((p, newBuf))
    }

    val newRdd = new RDD[(K, Iterable[T])](newGroupedPartitions.toMap, partitioner.numPartitions)

    RDDRepo.addNewWithEdgePartitioned(new PartitionedRDD[K, Iterable[T]](newRdd, Some(partitioner)), rid, "groupBy")
  }

  def keyBy[K: ClassTag](f: T => K) : PartitionedRDD[K, T] = {
    var keydPartitions = mutable.Map[RddTypes.PID, Buffer[(K, T)]]()
    for (p <- partitions.keys) {
      for (t <- partitions.get(p).get) {
        val key = f(t)
        if (keydPartitions.contains(p)) {
          keydPartitions.get(p).get += ((key, t))
        } else {
          val buf = mutable.Buffer((key, t))
          keydPartitions += ((p, buf))
        }
      }
    }

    val newRdd = new RDD[(K, T)](keydPartitions.toMap, partitions.size)
    RDDRepo.addNewWithEdgePartitioned(new PartitionedRDD[K, T](newRdd, None), rid, "keyBy")
  }

  def glom() : RDD[Buffer[T]] = {
    val mappedForGlom = this.mapPartitions(iter => Iterator(iter.toBuffer), false)
    val glomResult = new RDD[Buffer[T]](mappedForGlom.partitions, mappedForGlom.numSlices)
    RDDRepo.addNewWithEdge(glomResult, mappedForGlom.rid, "glom")
    // To have glom in our tree, must create another RDD which is a duplicate of the mapPartitions one
  }

  // Pair RDD wrappers
  def groupByKey[K: ClassTag, V: ClassTag](partitioner: HashPartitioner) = {
    this.asPartitionedRDD[K, V]().groupByKey(partitioner)
  }

  def reduceByKey[K: ClassTag, V: ClassTag](partitioner: HashPartitioner, f: (V, V) => V) = {
    this.asPartitionedRDD[K, V]().reduceByKey(partitioner, f)
  }

  def cogroup[K: ClassTag, V: ClassTag, W: ClassTag](other: RDD[(K, W)]) = {
    this.asPartitionedRDD[K, V]().cogroup(other.asPartitionedRDD[K, W]())
  }

  def cogroup[K: ClassTag, V: ClassTag, W1: ClassTag, W2: ClassTag](other1: RDD[(K, W1)], other2: RDD[(K, W2)]) = {
    this.asPartitionedRDD[K, V]().cogroup(other1.asPartitionedRDD[K, W1](), other2.asPartitionedRDD[K, W2]())
  }

  def partitionBy[K: ClassTag, V: ClassTag](partitioner: Partitioner) = {
    this.asPartitionedRDD[K,V].partitionBy(partitioner)
  }

  def flatMapValues[K: ClassTag, V: ClassTag, U: ClassTag](f: V => TraversableOnce[U]) = {
    this.asPartitionedRDD[K,V].flatMapValues(f)
  }

  // Actions

  def collect() = {
    if (SHOULD_DEBUG) println(partitions)

    // Want the set to be ordered by pid
    var set = mutable.Buffer[T]()
    val listMap = ListMap(partitions.toSeq.sortBy(_._1):_*) // Ordering partitions by pid

    if (SHOULD_DEBUG) println(implicitly[ClassTag[T]].runtimeClass)
    for (v <- listMap.values) {
      set ++= (v)
    }
    set
  }





  // Equivalence-test assisting transforms
  // Do not require adding to graph
  def mapMess[U: ClassTag](f: T => U) : RDD[U] = {
    val shuffledPartitions = Random.shuffle(partitions)
    val newPartitions = for ((pid, partition) <- shuffledPartitions) yield (pid, partition.map(f))
    new RDD[U](newPartitions.toMap, numSlices)
  }



  // Comparison 'action'
  def compare(other: RDD[T])(implicit ord: Ordering[T]) = {
    var thisValues = partitions.values.flatten
    var otherValues = other.partitions.values.flatten

    println("This values: " + thisValues)
    println("Other values: " + otherValues)

    thisValues.toBuffer.sorted(ord) == otherValues.toBuffer.sorted(ord)
  }
}
