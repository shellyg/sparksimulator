package il.ac.tau.shellyg.spark.examples

import il.ac.tau.shellyg.spark.{HashPartitioner, SparkConf, SparkContext}

/**
  * Created by shelly on 22/01/16.
  */
object glomEx {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Spark glom")
    val sc = new SparkContext(conf)

    val a = sc.parallelize(1 to 9, 3)

    println(a.glom().collect)
    val b = a.map((_,1))
    val c = b.partitionBy(new HashPartitioner(7))
    println(c.glom().collect)
    // Result: Array[Array[(Int, Int)]] = Array(Array((7,1)), Array((1,1), (8,1)), Array((2,1), (9,1)), Array((3,1)), Array((4,1)), Array((5,1)), Array((6,1)))

  }
}
