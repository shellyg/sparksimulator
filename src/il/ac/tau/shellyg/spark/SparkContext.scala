package il.ac.tau.shellyg.spark

import scala.collection.mutable
import scala.reflect.ClassTag

/**
  * Created by shelly on 15/01/16.
  */

case class SparkContext(conf: SparkConf) {

  // TODO: Add all new RDDs to RDDRepo, not just from parallelize. Best to do it from the constructor

  val SHOULD_DEBUG = false

  def println(x : Any) = { Utils.println(x) }

  def stop(): Int = {0}

  def parallelize(range: Range, slices: Int) = { // TODO: Use the generic parallelize receiving an abstract sequence!

    val bufFromRange = mutable.Buffer[Int]()
    bufFromRange++=range

    val newRdd = new RDD[Int](genPartitions(bufFromRange, slices).toMap, slices)
    RDDRepo.addNew(newRdd, true)
  }


  def parallelize[T: ClassTag](list: List[T], slices: Int) : RDD[T] = {
    val bufFromList = mutable.Buffer[T]()
    bufFromList++=list

    RDDRepo.addNew(new RDD[T](genPartitions(bufFromList, slices).toMap, slices), true)
  }

  def parallelize[T: ClassTag](list: List[T]) : RDD[T] = {
    parallelize(list, 4) // TODO: Configurable default...
  }

  def genPartitions[T: ClassTag](seqBuf : mutable.Buffer[T], numSlices: Int) : mutable.Map[RddTypes.PID, mutable.Buffer[T]] = {
    // Positions defines the positions where the seqBuf will be sliced to achieve maximum balance
    def positions(length : Int, numSlices : Int) : Iterator[(Int, Int)] = {
      (0 until numSlices).iterator.map(i => {
        val start = ((i * length) / numSlices)
        val end = (((i+1) * length) / numSlices)
        (start, end)
      })
    }

    var partitions = mutable.Map[RddTypes.PID, mutable.Buffer[T]]()
    if (SHOULD_DEBUG) positions(seqBuf.size, numSlices).foreach(println)
    positions(seqBuf.size, numSlices).foreach({
      case (start, end) =>
        if (SHOULD_DEBUG) println(start + " -> " + end + ": " + seqBuf.slice(start, end))
        partitions += ((start, seqBuf.slice(start, end)))
    })

    // Add empty partitions until reaching numSlices:
    val maxPid : Int = partitions.keys.max
    for (p <- maxPid+1 to numSlices-1) {
      partitions += ((p.toInt, mutable.Buffer()))
    }

    partitions
  }


}
