package il.ac.tau.shellyg.spark

import il.ac.tau.shellyg.spark.RddTypes.RID

import scala.collection.mutable.HashMap
import scala.reflect.ClassTag

/**
  * Created by shelly on 17/01/16.
  */
object RDDRepo {
  val SHOULD_DEBUG = false

  var currentRid = 0

  val repo = new HashMap[RddTypes.RID, RDD[_]]
  val shuffleStatsRepo = new HashMap[RddTypes.RID, Map[String, Any]]
  val lineage = new HashMap[RddTypes.RPID, Set[RddTypes.RPID]]
  val graphAllTransforms = new Graph()
  val graphUserTransforms = new Graph()

  def getNextRid(): RID = {
    currentRid = currentRid+1
    currentRid
  }

  def addNew[T: ClassTag](newRdd: RDD[T], asRoot : Boolean = false, isUserTransform : Boolean = true) = {
    if (SHOULD_DEBUG) println("Adding new RDD with RID " + newRdd.rid)
    repo.put(newRdd.rid, newRdd)
//    if (asRoot)
      graphAllTransforms.add(newRdd.rid, asRoot)
    if (isUserTransform) {
      graphUserTransforms.add(newRdd.rid, asRoot)
    }
    newRdd // Returns the new RDD
    /*
      for partition : getPartitions(newRdd)

        lineage.+(((currentRid,partition), new Set()))
     */

  }

  def addNewPartitioned[K: ClassTag, V: ClassTag](newRdd : PartitionedRDD[K,V], isUserTransform : Boolean = true) = {
    addNew[(K,V)](newRdd.self, isUserTransform)
    newRdd
  }

  def findLowestCommonAncestor(rp: RID) = {

    // Ancestor must have rid < rp
    // So DFS from each root rid in user graph which is < rp but in the "All" graph
    // Take the one with the minimal number of hops
    val userRoots = graphUserTransforms.roots

    var minHops = rp /* rp is large enough */
    var lowestCommonAncestor = userRoots.head
    var found = false

    for (rootRid <- userRoots) { // Taking self/parents until finding one in the user transform
      if (!found) {
        var candidateLca = rp
        val traversal = graphUserTransforms.dfs(rootRid)
        var prevCandidate = RddTypes.INVALID_RID
        if (SHOULD_DEBUG) println("Candidate lca is " + candidateLca + " and traversal is " + traversal)
        while (!traversal.map((e) => e._2).contains(candidateLca) && (prevCandidate != candidateLca)) {
          prevCandidate = candidateLca
          candidateLca = graphAllTransforms.parent(candidateLca)
        }

        if (traversal.map((e) => e._2).contains(candidateLca)) {
          found = true
          lowestCommonAncestor = candidateLca
        }
      }
    }

    lowestCommonAncestor
  }

  def addNewWithEdge[T: ClassTag](newRdd: RDD[T], rp : RID, transform : String, isUserTransform : Boolean = true) = {
    // If rp does not exist in the repo, just ignore this operation
    if (repo.get(rp) != None) {
      if (SHOULD_DEBUG) println("Adding new RDD with edge from RID " + rp + " to new " + newRdd.rid + " with edge " + transform)
      repo.put(newRdd.rid, newRdd)
      graphAllTransforms.addEdge(rp, newRdd.rid, transform)
      if (isUserTransform) {
        val rpUser = if (findLowestCommonAncestor(rp) == rp) rp else findLowestCommonAncestor(rp)
        if (SHOULD_DEBUG) println("User transfrom: " + transform + ", from " + rpUser + " to " + newRdd.rid)
        graphUserTransforms.addEdge(rpUser, newRdd.rid, transform)
      }
    }
    newRdd
  }

  def addNewWithEdgePartitioned[K: ClassTag, V: ClassTag](newRdd : PartitionedRDD[K,V], rp : RID, transform : String, isUserTransform : Boolean = true) = {
    addNewWithEdge[(K, V)](newRdd.self, rp, transform, isUserTransform)
    newRdd
  }

  def addShuffleStats(r : RID, stats: Map[String, Any]) = {
    shuffleStatsRepo.put(r, stats)
  }

  def getRoots() = {
    graphAllTransforms.roots
  }

  def traverse(r : RID) = {
    if (SHOULD_DEBUG) println("Traversing, starting from root with RID " + r)
    if (SHOULD_DEBUG) println("Underlying full structure = " + graphAllTransforms.g)
    if (SHOULD_DEBUG) println("Underlying user structure = " + graphUserTransforms.g)
    graphAllTransforms.dfs(r)
  }

}
