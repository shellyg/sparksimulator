package il.ac.tau.shellyg.spark.examples

// scalastyle:off println
import il.ac.tau.shellyg.spark._

/** Computes an approximation to pi */
object UnionEx {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Spark Pi")
    val sc = new SparkContext(conf)

    val a = sc.parallelize(1 to 3, 1)
    val b = sc.parallelize(5 to 7, 1)
    println("-------------------TEST1--------------------")
    println(a.union(b).collect())
    println((a ++ b).collect())
    // Result: 1,2,3,5,6,7

    val c = sc.parallelize(5 to 7, 2)
    println("-------------------TEST2--------------------")
    println(a.union(c).collect())
    // Result: 5,6,7

    val d = sc.parallelize(1 to 3, 3)
    val e = sc.parallelize(5 to 7, 7)
    println("-------------------TEST3--------------------")
    println((d ++ e).collect())
    // Result: 1,2,3,5,6,7

    // Key-Value RDDs
    val x1 = a.cartesian(a)
    val x2 = b.cartesian(b)
    println("-------------------TEST4--------------------")
    println(x1.union(x2).glom.collect())
    // Result: (1,1),(1,2),(1,3),(2,1),(2,2),(2,3),(3,1),(3,2),(3,3), (5,5),(5,6),(5,7),(6,5),(6,6),(6,7),(7,5),(7,6),(7,7)

    val x1b = x1.partitionBy(new HashPartitioner(3))
    val x2b = x2.partitionBy(new HashPartitioner(7))
    println("-------------------TEST5--------------------")
    println(x1b.union(x2b).glom.collect)
    // Result: Array[Array[(Int, Int)]] =
    // Array(Array((3,1), (3,2), (3,3)),
    //    Array((1,1), (1,2), (1,3)),
    //    Array((2,1), (2,2), (2,3)),
    //    Array((7,5), (7,6), (7,7)),
    //    Array(),
    //    Array(),
    //    Array(),
    //    Array(),
    //    Array((5,5), (5,6), (5,7)),
    //    Array((6,5), (6,6), (6,7)))

    val x1c = x1.partitionBy(new HashPartitioner(2))
    val x2c = x2.partitionBy(new HashPartitioner(2))
    println("-------------------TEST6--------------------")
    println(x1c.union(x2c).glom.collect)
    // Result: Array(Array((2,1), (2,2), (2,3), (6,5), (6,6), (6,7)), Array((1,1), (1,2), (1,3), (3,1), (3,2), (3,3), (5,5), (5,6), (5,7), (7,5), (7,6), (7,7)))


    // Multi argument union
    val ab = sc.parallelize(10 to 13, 1)
    println("-------------------TEST7--------------------")
    println((a ++ b ++ ab).collect()) // Will not call union on a sequence
    println(a.union(b, ab).collect) // Will call union on a sequence
    println(x1c.union(x2c, x1c).glom.collect)
    sc.stop()
  }
}

// scalastyle:on println
