package il.ac.tau.shellyg.spark.examples.partitioning

import il.ac.tau.shellyg.spark.{Partitioner, Utils, RddTypes}

/**
  * Created by shelly on 25/02/16.
  */
class FirstLetterPartitioner() extends Partitioner {

  def numPartitions: Int = 26

  def getPartition(key: Any): RddTypes.PID = key match {
    case null => 0
    case key: String => if (key.length() == 0) 1 else Math.max(key.toLowerCase.charAt(0).toInt - 96, 1)
    case _ => 0
  }

  override def equals(other: Any): Boolean = other match {
    case h: FirstLetterPartitioner =>
      true
    case _ =>
      false
  }

  override def hashCode: Int = numPartitions
}

class TwoFirstLettersPartitioner() extends Partitioner {
  def numPartitions: Int = 26

  def getPartition(key : Any) : RddTypes.PID = key match {
    case null => 0
    case key: String => if (key.length() == 0) 1 else Math.max((key.toLowerCase.charAt(0).toInt + key.toLowerCase.charAt(Math.min(1, key.length-1)).toInt - 96*2) % 26 + 1, 1)
    case _ => 0
  }

  override def equals(other: Any): Boolean = other match {
    case h: TwoFirstLettersPartitioner =>
      true
    case _ =>
      false
  }

  override def hashCode: Int = numPartitions
}

