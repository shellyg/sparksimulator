package il.ac.tau.shellyg.spark.examples

// scalastyle:off println
import il.ac.tau.shellyg.spark._

/** Computes an approximation to pi */
object FlatMapValuesEx {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Spark Pi")
    val sc = new SparkContext(conf)

    val a = sc.parallelize(List("dog", "tiger", "lion", "cat", "panther", "eagle"), 2)
    val b = a.map(x => (x.length, x))
    println(b.flatMapValues[Int,String,Char]("x" + _ + "x").collect)
    // Result: Array((3,x), (3,d), (3,o), (3,g), (3,x),
    // (5,x), (5,t), (5,i), (5,g), (5,e), (5,r), (5,x),
    // (4,x), (4,l), (4,i), (4,o), (4,n), (4,x),
    // (3,x), (3,c), (3,a), (3,t), (3,x),
    // (7,x), (7,p), (7,a), (7,n), (7,t), (7,h), (7,e), (7,r), (7,x),
    // (5,x), (5,e), (5,a), (5,g), (5,l), (5,e), (5,x))

    sc.stop()
  }
}

// scalastyle:on println
