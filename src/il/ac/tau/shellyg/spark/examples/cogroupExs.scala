package il.ac.tau.shellyg.spark.examples

// scalastyle:off println
import il.ac.tau.shellyg.spark._

/** Computes an approximation to pi */
object cogroupExs {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Spark Pi")
    val sc = new SparkContext(conf)

    val a = sc.parallelize(List(1, 2, 1, 3), 1)
    val b = a.map((_, "b"))
    val c = a.map((_, "c"))
    println(b.cogroup(c).collect)
    /*
    Result:
        Array(
    (2,(ArrayBuffer(b),ArrayBuffer(c))),
    (3,(ArrayBuffer(b),ArrayBuffer(c))),
    (1,(ArrayBuffer(b, b),ArrayBuffer(c, c)))
    )
     */

    val d = a.map((_, "d"))
    println(b.cogroup(c, d).collect)
    /* Result: Array[(Int, (Iterable[String], Iterable[String], Iterable[String]))] = Array(
    (2,(ArrayBuffer(b),ArrayBuffer(c),ArrayBuffer(d))),
    (3,(ArrayBuffer(b),ArrayBuffer(c),ArrayBuffer(d))),
    (1,(ArrayBuffer(b, b),ArrayBuffer(c, c),ArrayBuffer(d, d)))
    )
    */

    val x = sc.parallelize(List((1, "apple"), (2, "banana"), (3, "orange"), (4, "kiwi")), 2)
    val y = sc.parallelize(List((5, "computer"), (1, "laptop"), (1, "desktop"), (4, "iPad")), 2)
    println(x.cogroup(y).collect)
    /* Result: Array[(Int, (Iterable[String], Iterable[String]))] = Array(
                    (4,(ArrayBuffer(kiwi),ArrayBuffer(iPad))),
                    (2,(ArrayBuffer(banana),ArrayBuffer())),
                    (3,(ArrayBuffer(orange),ArrayBuffer())),
                    (1,(ArrayBuffer(apple),ArrayBuffer(laptop, desktop))),
                    (5,(ArrayBuffer(),ArrayBuffer(computer))))
     */

    sc.stop()
  }
}

// scalastyle:on println
