package il.ac.tau.shellyg.spark.examples

// scalastyle:off println
import il.ac.tau.shellyg.spark._

/** Computes an approximation to pi */
object FlatMapExs {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Spark Pi")
    val sc = new SparkContext(conf)

    val a = sc.parallelize(1 to 10, 5)
    println(a.flatMap(1 to _).collect)
    // Wanted: Array      (1, 1, 2, 1, 2, 3, 1, 2, 3, 4, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    // Actual: ArrayBuffer(1, 1, 2, 1, 2, 3, 1, 2, 3, 4, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

    println(sc.parallelize(List(1, 2, 3), 2).flatMap(x => List(x, x, x)).collect)
    // Wanted: Array(1, 1, 1, 2, 2, 2, 3, 3, 3)
    // Actual: ArrayBuffer(3, 3, 3, 2, 2, 2, 1, 1, 1)

    val x  = sc.parallelize(1 to 10, 3)
    println(x.flatMap(List.fill(scala.util.Random.nextInt(10))(_)).collect)
    // Random # of copies, but why not ordered?

    sc.stop()
  }
}

// scalastyle:on println
