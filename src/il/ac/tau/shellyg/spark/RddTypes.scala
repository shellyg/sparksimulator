package il.ac.tau.shellyg.spark

/**
  * Created by shelly on 17/01/16.
  */
package object RddTypes {

  type RID = Integer
  type PID = Integer
  type RPID = (RID, PID)

  val INVALID_RID = -1
}
