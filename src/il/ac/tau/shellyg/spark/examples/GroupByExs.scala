package il.ac.tau.shellyg.spark.examples

// scalastyle:off println
import il.ac.tau.shellyg.spark._

/** Computes an approximation to pi */
object GroupByExs {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Spark Pi")
    val sc = new SparkContext(conf)

    val a = sc.parallelize(1 to 9, 3)
    val partitioner = new HashPartitioner(6)

    println(a.groupBy(x => { if (x % 2 == 0) "even" else "odd" }, partitioner).collect)
    // Result: Array((even,ArrayBuffer(2, 4, 6, 8)), (odd,ArrayBuffer(1, 3, 5, 7, 9)))

    def myfunc(a: Int) : Int =
    {
      a % 2
    }

    println(a.groupBy(myfunc, partitioner).collect)
    // Result: Array((0,ArrayBuffer(2, 4, 6, 8)), (1,ArrayBuffer(1, 3, 5, 7, 9)))

    println(a.groupBy(myfunc(_), new HashPartitioner(1)).collect)
    // Result: Array((0,ArrayBuffer(2, 4, 6, 8)), (1,ArrayBuffer(1, 3, 5, 7, 9)))

    class MyPartitioner extends Partitioner {
      def numPartitions: Int = 2
      def getPartition(key: Any): RddTypes.PID =
      {
        key match
        {
          case null     => 0
          case key: Int => key          % numPartitions
          case _        => key.hashCode % numPartitions
        }
      }
      override def equals(other: Any): Boolean =
      {
        other match
        {
          case h: MyPartitioner => true
          case _                => false
        }
      }
    }

    val p = new MyPartitioner()
    val b = a.groupBy((x:Int) => { x }, p)
//    val c = b.mapWith(i => i)((a, b) => (b, a)) // TODO: Implement mapWith
    println(b.collect())
    // Result: Array((0,(4,ArrayBuffer(4))), (0,(2,ArrayBuffer(2))), (0,(6,ArrayBuffer(6))), (0,(8,ArrayBuffer(8))), (1,(9,ArrayBuffer(9))), (1,(3,ArrayBuffer(3))), (1,(1,ArrayBuffer(1))), (1,(7,ArrayBuffer(7))), (1,(5,ArrayBuffer(5))))


    sc.stop()
  }
}

// scalastyle:on println
