package il.ac.tau.shellyg.spark.examples

import il.ac.tau.shellyg.spark.{SparkContext, SparkConf}

/**
  * Created by shelly on 22/01/16.
  */
object mapPartitionsEx {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Spark mapPartitions")
    val sc = new SparkContext(conf)

    val a = sc.parallelize(1 to 9, 3)
    def myfunc[T](iter: Iterator[T]): Iterator[(T, T)] = {
      var res = List[(T, T)]()
      var pre = iter.next
      while (iter.hasNext) {
        val cur = iter.next;
        res.::=(pre, cur)
        pre = cur;
      }
      res.iterator
    }

    println(a.mapPartitions(myfunc).collect)
    // Result: Array((2,3), (1,2), (5,6), (4,5), (8,9), (7,8))
  }
}
