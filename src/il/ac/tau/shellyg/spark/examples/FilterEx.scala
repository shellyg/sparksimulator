package il.ac.tau.shellyg.spark.examples


// scalastyle:off println
import il.ac.tau.shellyg.spark._

import scala.math.random

/** Computes an approximation to pi */
object FilterEx {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Spark Pi")
    val sc = new SparkContext(conf)
    val a = sc.parallelize(1 to 10, 3)
    val b :RDD[Int] = a.filter(_ % 2 == 0)
    println("a = " + a.collect())
    println ("b = " + b.collect())

    //println(a.cartesian(b).collect())
    sc.stop()
  }
}

// scalastyle:on println
