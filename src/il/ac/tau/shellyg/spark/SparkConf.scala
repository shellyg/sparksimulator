package il.ac.tau.shellyg.spark

/**
  * Created by shelly on 15/01/16.
  */
case class SparkConf() {
  var appName = "generic"

  def setAppName(s: String) :SparkConf = {
    appName = s
    this
  }

}
