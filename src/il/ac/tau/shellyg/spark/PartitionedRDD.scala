
package il.ac.tau.shellyg.spark
//
import il.ac.tau.shellyg.spark.RddTypes.PID
import il.ac.tau.shellyg.spark.examples.partitioning.FirstLetterPartitioner

import scala.collection.mutable
import scala.collection.parallel.immutable
import scala.reflect.ClassTag
import scala.collection.mutable.Buffer
//
/**
  * Created by shelly on 22/01/16.
  */
/*
 * self defines the RDD's data
 * partitioner will only be defined when self is partitioned according to partitioner.
 * A None partitioner means there are no guarantees on how data in self is partitioned
 */
class PartitionedRDD[K, V](val self: RDD[(K,V)], val partitioner: Option[Partitioner])
(implicit val kt: ClassTag[K], val vt: ClassTag[V]) {

  val SHOULD_DEBUG = false

  def println(x : Any) = { Utils.println(x) }

  private def keyClass : Class[_] = kt.runtimeClass
  private def valueClass : Class[_] = vt.runtimeClass

  def partitions = {self.partitions}

  def reduceByKey[T: ClassTag](partitioner: Partitioner, f: (T, T) => T) : PartitionedRDD[K, V] = {
    this.asInstanceOf[PartitionedRDD[K, T]].reduceByKey(partitioner, f).asInstanceOf[PartitionedRDD[K, V]]
  }

  def reduceByKey(partitioner: Partitioner, f: (V, V) => V) : PartitionedRDD[K, V] = {
    def fReduce(partition: Iterator[(K, V)]) : Iterator[(K, V)] = {
      var map = mutable.Map[K, V]()

      if (SHOULD_DEBUG) println("Got partition: " + partition)
      while (partition.hasNext) {
        val pre = partition.next
        val key = pre._1
        val value = pre._2

        if (map.contains(key)) {
          map += ((key, f(map.get(key).get, value)))
        } else {
          map += ((key, value))
        }
      }
      map.toIterator
    }

    // First step mapPartitions:
    val firstMapPartitions = new PartitionedRDD(self.mapPartitions[(K,V)](fReduce, false), None)
    val shuffled = firstMapPartitions.shuffle(partitioner)
    val mappedShuffled = shuffled.self.mapPartitions(fReduce, false)
    val finalRdd = new PartitionedRDD(mappedShuffled, Some(partitioner))
    if (SHOULD_DEBUG) {
      println("First map partitions: " + firstMapPartitions.collect())
      println("Shuffled: " + shuffled.collect())
      println("Final RDD: " + finalRdd.collect())
    }

    RDDRepo.addNewWithEdgePartitioned(finalRdd, shuffled.self.rid, "reduceByKey")
  }

  def groupByKey(partitioner : Partitioner) : PartitionedRDD[K, Iterable[V]] = {
    this.shuffle(partitioner).groupByKey()
  }

  // Assumes already partitioned
  def groupByKey() : PartitionedRDD[K, Iterable[V]] = {
    def fGroup(partition : Iterator[(K, V)]) : Iterator[(K, Iterable[V])] = {
      var groupedBufMap = mutable.Map[K, mutable.Buffer[V]]()
      for (elem <- partition) {
        if (groupedBufMap.contains(elem._1)) {
          groupedBufMap.get(elem._1).get += elem._2

        } else {
          var newIterable = mutable.Buffer[V]()
          newIterable += elem._2
          groupedBufMap += ((elem._1, newIterable))
        }
      }

      // Now convert to proper type
      groupedBufMap.mapValues(_.toIterable).toIterator
    }

    val newRdd : RDD[(K, Iterable[V])] = self.mapPartitions(fGroup, false)
    val newRddForPartitioned = new RDD[(K, Iterable[V])](newRdd.partitions, newRdd.numSlices)
    val newRddPartitioned = new PartitionedRDD[K, Iterable[V]](newRddForPartitioned, partitioner)
    if (SHOULD_DEBUG) println("newRdd.rid = " + newRdd.rid + ", newRddForPartitioned.rid = " + newRddForPartitioned.rid)
    RDDRepo.addNewWithEdgePartitioned(newRddPartitioned, newRdd.rid, "groupByKey") // Use this partitioner's, as the assumption it is already partitioned
  }

  def cogroup[W: ClassTag](other: PartitionedRDD[K, W]) : PartitionedRDD[K, (Iterable[V], Iterable[W])] = {
    RDDRepo.addNewWithEdgePartitioned(cogroup(other, DefaultPartitioner.defaultPartitioner(this, other)), self.rid, "cogroup")
  }

  def cogroup[W1: ClassTag, W2: ClassTag](other1: PartitionedRDD[K, W1], other2: PartitionedRDD[K, W2])
    : PartitionedRDD[K, (Iterable[V], Iterable[W1], Iterable[W2])] = {
    RDDRepo.addNewWithEdgePartitioned(cogroup(other1, other2, DefaultPartitioner.defaultPartitioner(this, other1, other2)), self.rid, "cogroup")
  }

  // Wonder how to remove code duplication? maybe with zip?
  def cogroup[W1: ClassTag, W2: ClassTag](other1: PartitionedRDD[K, W1], other2: PartitionedRDD[K, W2], partitioner: Partitioner)
        : PartitionedRDD[K, (Iterable[V], Iterable[W1], Iterable[W2])] = {
    def mapToFirst(elem: (K,V)) : (K, (Option[V], Option[W1], Option[W2])) = {
      (elem._1, (Some(elem._2), None, None))
    }

    def mapToSecond(elem: (K,W1)) : (K, (Option[V], Option[W1], Option[W2])) = {
      (elem._1, (None, Some(elem._2), None))
    }

    def mapToThird(elem: (K, W2)) : (K , (Option[V], Option[W1], Option[W2])) = {
      (elem._1, (None, None, Some(elem._2)))
    }

    val thisMapped = self.mapToPair(mapToFirst)
    val other1Mapped = other1.self.mapToPair(mapToSecond)
    val other2Mapped = other2.self.mapToPair(mapToThird)

    val uniteMappedOnes = thisMapped.union(other1Mapped,other2Mapped)
    val groupedUnitedMappedOnes = uniteMappedOnes.groupByKey(partitioner)

    def getRidOfNones(elem: (Iterable[(Option[V], Option[W1], Option[W2])])) : (Iterable[V], Iterable[W1], Iterable[W2]) = {
      val iterator = elem.iterator
      var vBuf = mutable.Buffer[V]()
      var w1Buf = mutable.Buffer[W1]()
      var w2Buf = mutable.Buffer[W2]()
      while (iterator.hasNext) {
        val curr = iterator.next()

        if (curr._1 != None) {
          vBuf ++= (curr._1)
        }

        if (curr._2 != None) {
          w1Buf ++= (curr._2)
        }

        if (curr._3 != None) {
          w2Buf ++= (curr._3)
        }

      }

      (vBuf.toIterable, w1Buf.toIterable, w2Buf.toIterable)
    }

    RDDRepo.addNewWithEdgePartitioned(groupedUnitedMappedOnes.mapValues[(Iterable[V], Iterable[W1], Iterable[W2])](getRidOfNones, false), self.rid, "cogroup")
  }

  def cogroup[W: ClassTag](other: PartitionedRDD[K, W], partitioner: Partitioner) : PartitionedRDD[K, (Iterable[V], Iterable[W])] = {
    // Map this RDD (K,V) to (K, (Some(V), None(W))
    def mapToFirst(elem: (K,V)) : (K, (Option[V], Option[W])) = {
      (elem._1, (Some(elem._2), None))
    }
    val thisMapped = self.mapToPair[K, (Option[V], Option[W])](mapToFirst)
    if (SHOULD_DEBUG) println("cogroup: This mapped: " + thisMapped.collect())

    // Map other RDD (K,W) to (K, (None(V), Some(W)))
    def mapToSecond(elem: (K,W)) : (K, (Option[V], Option[W])) = {
      (elem._1, (None, Some(elem._2)))
    }
    val otherMapped = other.self.mapToPair[K, (Option[V], Option[W])](mapToSecond)
    if (SHOULD_DEBUG) println("cogroup: Other mapped: " + otherMapped.collect())

    // Group the mapped ones to (K, Iterable[(Option[V], Option[W])]
    val uniteMappedOnes = thisMapped.union(otherMapped)
    if (SHOULD_DEBUG) println("cogroup: Unite mapped ones: " + uniteMappedOnes.collect())
    val groupedUnitedMappedOnes = uniteMappedOnes.groupByKey(partitioner)
    if (SHOULD_DEBUG) println("cogroup: Grouped united mapped ones: " + groupedUnitedMappedOnes.collect())

    // Map to get a pair of iterables: (Iterable[V], Iterable[W]) by taking each value, building 2 sequences, and joining to a tuple
    def getRidOfNones(elem: (Iterable[(Option[V], Option[W])])) : (Iterable[V], Iterable[W]) = {
      val iterator = elem.iterator
      var vBuf = mutable.Buffer[V]()
      var wBuf = mutable.Buffer[W]()
      while (iterator.hasNext) {
        val curr = iterator.next()

        if (curr._1 != None) {
          vBuf ++= (curr._1)
        }

        if (curr._2 != None) {
          wBuf ++= (curr._2)
        }

      }

      (vBuf.toIterable, wBuf.toIterable)
    }

    RDDRepo.addNewWithEdgePartitioned(groupedUnitedMappedOnes.mapValues[(Iterable[V], Iterable[W])](getRidOfNones, false), self.rid, "cogroup")
  }

  // Only updates values.
  def mapValues[U: ClassTag](f: V => U, isFromUser : Boolean = true) : PartitionedRDD[K, U] = {
    RDDRepo.addNewWithEdgePartitioned(new PartitionedRDD[K, U](self.mapPartitions(iter => iter.map { case (k, v) => (k, f(v)) }, false), partitioner), self.rid, "mapValues", isFromUser)
  }

  // The canonical shuffle
  def partitionBy(partitioner: Partitioner) = {
    RDDRepo.addNewWithEdgePartitioned(shuffle(partitioner), self.rid, "partitionBy")
  }


  /* Collect Stats on how much data goes from which partition to what other partition.
  We would like to optimize the size of partitions to be balanced and the location of the partitions.
  1. We can calculate balance as the standard deviation from the average number of expected elements per partition.
  2. To optimize the positioning (location), we need to guess to which node which partition goes.
  2.1 First, calculate the connection of out_partition -> originating partition, when originating partition is picked
      for each out_partition as the partition that is responsible for the largest amount of  data in the out_partition.
  2.2 This may still lead to imbalance as one large original partitioner may have its hosting node host many of the out_partititions.
      So calculate also the imbalance when grouping together all out_partitions linked to some originating_partition and calculating
      the imbalance compared to even spread among #original partitions.
  2.3 Disregarding the imbalance, use the data to calculate how many records are communicated to the network.
      In each original_partition, all records that do not map to an out_partition which maps to the same original_partition,
      is counted as a record which is communicated.
  */
  def calcStats(newRdd : RDD[(K, V)], movementRecorder: Map[(RddTypes.PID, RddTypes.PID), Int]) = {
    // Calc imbalance over partitions
    val newPartitions = newRdd.partitions
    val size = newPartitions.values.foldLeft[Int](0)((current : Int, buffer : mutable.Buffer[(K,V)]) => (current + buffer.size))
    val meanSize = size.toDouble / newPartitions.keys.size
    // The standard deviation is the square root of the variance, which is the mean of sum of differences
    val variance = newPartitions.values.map((b) => Math.pow((b.size - meanSize),2)).sum / newPartitions.keys.size
    val stdDeviation = Math.sqrt(variance)
    // Max size of a partition:
    val maxPartitionSize = newPartitions.values.maxBy((b) => b.size).size

    val oldSize = self.partitions.values.foldLeft[Int](0)((current : Int, buffer : mutable.Buffer[(K, V)]) => (current + buffer.size))
    val oldMeanSize = oldSize.toDouble / self.partitions.keys.size
    val oldStdDeviation = Math.sqrt(self.partitions.values.map((b) => Math.pow((b.size - oldMeanSize),2)).sum / self.partitions.keys.size)
    val oldMaxPartitionSize = self.partitions.values.maxBy((b) => b.size).size

    if (SHOULD_DEBUG) println("Size of shuffled RDD is: " + size + " with " + newPartitions.keys.size +
      " partitions and Mean size of a partition in the shuffled RDD is " + meanSize +
      ",\nMax partitions size is " + maxPartitionSize + ", and the standard deviation is " + stdDeviation +
      ". previous max partition size is " + oldMaxPartitionSize + " (" + self.partitions.keys.size + " partitions) and standard deviation is " + oldStdDeviation)

    if (SHOULD_DEBUG) println("Movement recorder: " + movementRecorder)
    // Mapping targets to their origins
    val outToOriginMap = mutable.Map[RddTypes.PID, (Int, RddTypes.PID)]()

    for ((i,j) <- movementRecorder.keys) {
      if (SHOULD_DEBUG) println("Working on " + i + ", " + j + " with movement record = " + movementRecorder.get((i,j)))
      val currentEntry = outToOriginMap.get(j).getOrElse((0, i))
      if (SHOULD_DEBUG) println("Current entry: " + currentEntry)

      val newSum = currentEntry._1 + movementRecorder.get((i, j)).get
      val newMax = if (movementRecorder.get((currentEntry._2), j).get > movementRecorder.get((i, j)).get) {
        currentEntry._2
      } else {
        i
      }

      outToOriginMap.put(j, (newSum, newMax))
    }

    if (SHOULD_DEBUG) println("Out to Origin map: " + outToOriginMap)

    // Calculate the imbalance when grouping new partitions to their origins ( = machines)
    val dataOnMachine = mutable.Map[RddTypes.PID, Int]()
    for (j <- outToOriginMap.keys) {
      val sizeOfOutPartitionAndOrigin = outToOriginMap.get(j).get
      val size = dataOnMachine.get(sizeOfOutPartitionAndOrigin._2).getOrElse(0)
      dataOnMachine.put(sizeOfOutPartitionAndOrigin._2, size + sizeOfOutPartitionAndOrigin._1)
    }

    if (SHOULD_DEBUG) println("How much data there is on a machine: " + dataOnMachine)
    val totalSizeOnMachines = dataOnMachine.values.sum
    val meanSizeOnMachine = dataOnMachine.values.sum.toDouble / dataOnMachine.keys.size
    val stdDeviationOnMachine = Math.sqrt(dataOnMachine.values.map((s) => Math.pow((s - meanSizeOnMachine),2)).sum / dataOnMachine.keys.size)
    val maxSizeOfDataOnMachine = dataOnMachine.values.max
    if (SHOULD_DEBUG) println("Size of data on all machines is " + totalSizeOnMachines + ", mean size on machine is " + meanSizeOnMachine + "," +
      "\nThe standard deviation is " + stdDeviationOnMachine + " and the max amount of data on a single machine is " + maxSizeOfDataOnMachine)

    // Calculate the amount of communication: In outToOrigin (j->(Sum, i)) this is Sum - i's contribution
    val communication = outToOriginMap.keys.map((j :RddTypes.PID) => (outToOriginMap.apply(j)._1 - movementRecorder.get((outToOriginMap.apply(j)._2, j)).get)).sum
    if (SHOULD_DEBUG) println("Communication: " + communication)

    val stats = mutable.Map[String, Any]()
    stats.put("movement", movementRecorder)
    stats.put("outToOrigin", outToOriginMap)
    stats.put("dataOnEachMachine", dataOnMachine)
    stats.put("oldSize", oldSize)
    stats.put("oldMeanSize", oldMeanSize)
    stats.put("oldStdDeviation", oldStdDeviation)
    stats.put("oldMaxPartitionSize", oldMaxPartitionSize)
    stats.put("newSize", size)
    stats.put("newMeanSize", meanSize)
    stats.put("newStdDeviation", stdDeviation)
    stats.put("newMaxPartitionSize", maxPartitionSize)
    stats.put("totalSizeOnMachines", totalSizeOnMachines)
    stats.put("meanSizeOnMachine", meanSizeOnMachine)
    stats.put("stdDeviationOnMachine", stdDeviationOnMachine)
    stats.put("maxSizeOfDataOnMachine", maxSizeOfDataOnMachine)
    stats.put("communication", communication)

    RDDRepo.addShuffleStats(newRdd.rid, stats.toMap)
  }


  // Shuffles keys together to exist on the same partition
  def shuffle(newPartitioner: Partitioner) = {
    var movementRecorder = mutable.Map[(RddTypes.PID, RddTypes.PID), Int]()

    var newPartitions = mutable.Map[RddTypes.PID, Buffer[(K,V)]]()

    for (p <- self.partitions.keys) {
      // Loop on partitions
      val buf = self.partitions.get(p).get
      val keydBuf = buf.map(t => t.asInstanceOf[(K,V)]._1 -> t)
      // Loop on keyd buf:
      for (elem <- keydBuf) {
        val currentKey = elem._1
        val elemToBeInserted = elem._2.asInstanceOf[(K,V)]
        val newPid = newPartitioner.getPartition(currentKey)
        if (SHOULD_DEBUG) println("Hash code for " + currentKey + " is " + currentKey.hashCode)
        if (SHOULD_DEBUG) println("Partition for key " + currentKey + " is " + newPid)
        if (newPartitions.get(newPid) == None) {
          newPartitions += ((newPid, mutable.Buffer[(K, V)]()))
        }
        newPartitions.get(newPid).get += elemToBeInserted

        // elemToBeInserted moved from p to newPid.
        if (SHOULD_DEBUG) println("Movement from " + p + " to " + newPid)
        Utils.addMovement(p, newPid, movementRecorder)
      }
    }

    val newRDD = new RDD[(K,V)](newPartitions.toMap, newPartitioner.numPartitions)
    calcStats(newRDD, movementRecorder.toMap)
    RDDRepo.addNewWithEdgePartitioned(new PartitionedRDD[K,V](newRDD, Some(newPartitioner)), self.rid, "shuffle", false)
  }

  def union(others : Product) : PartitionedRDD[K, V] = {
    RDDRepo.addNewWithEdgePartitioned(others.productIterator.toSeq.asInstanceOf[Seq[PartitionedRDD[K,V]]].foldLeft(this.asInstanceOf[PartitionedRDD[K,V]])(_ ++ _), self.rid, "union")
  }

  def union[K1: ClassTag, V1: ClassTag](other: PartitionedRDD[K1, V1]) : PartitionedRDD[K, V] = {
    if (this.partitioner != None && Some(this.partitioner) == Some(other.partitioner)) {
      if (SHOULD_DEBUG) println("Partitioned aware union!")
      var newPartitions = mutable.Map[RddTypes.PID, mutable.Buffer[(K, V)]]()
      for (p <- partitions.keys) {
        if (SHOULD_DEBUG) println("Adding " + self.partitions.get(p).get)
        newPartitions += ((p.toInt, self.partitions.get(p).get))

        if (SHOULD_DEBUG) println("Adding " + other.self.partitions.get(p).get)
        newPartitions.get(p).get ++= other.asInstanceOf[PartitionedRDD[K,V]].self.partitions.get(p).get
      }

      RDDRepo.addNewWithEdgePartitioned(new PartitionedRDD[K, V](new RDD[(K,V)](newPartitions.toMap, partitioner.get.numPartitions), partitioner), self.rid, "union")
    } else {
      RDDRepo.addNewWithEdgePartitioned(new PartitionedRDD(self.union(other.asInstanceOf[PartitionedRDD[K, V]].self, false), partitioner), self.rid, "union")
    }
  }

  def ++(other: PartitionedRDD[K, V]) : PartitionedRDD[K, V] = {this.union(other)}

  def flatMapValues[W: ClassTag](f: V => TraversableOnce[W]) : PartitionedRDD[K, W] = {
    // Preserves partitioning
    val newRdd = self.mapPartitions[(K, W)](iter => iter.flatMap { case (k, v) =>
      f(v).map(x => (k, x))}, false)
    RDDRepo.addNewWithEdgePartitioned(new PartitionedRDD[K, W](newRdd, partitioner), self.rid, "flatMapValues")
  }

  def join[W: ClassTag](other: PartitionedRDD[K, W], partitioner: Partitioner) : PartitionedRDD[K, (V,W)] = {
    RDDRepo.addNewWithEdgePartitioned(
      this.cogroup(other, partitioner).flatMapValues[(V,W)]( pair =>
      for (v <- pair._1.iterator; w <- pair._2.iterator) yield (v, w)
    ), self.rid, "join")
  }

  def join[W: ClassTag](other: PartitionedRDD[K, W]) : PartitionedRDD[K, (V,W)] = {
    RDDRepo.addNewWithEdgePartitioned(join(other, DefaultPartitioner.defaultPartitioner(this, other)), self.rid, "join")
  }


  // Implementing functions from the original RDD class...]
  def glom() = { self.glom() }
  def collect() = { self.collect() }
  def map[U: ClassTag](f: Tuple2[Any, Any] => U) : RDD[U] = {
    def g(x : (K,V)) : U = {
      f((x._1, x._2))
    }
    self.map(g)
  }
}
