package il.ac.tau.shellyg.spark

import il.ac.tau.shellyg.spark.RddTypes.{RID, PID}

import scala.collection.mutable

/**
  * Created by shelly on 22/01/16.
  * Copied in part from org.apache.spark.util.Utils
  */
object Utils {

  def println(x : Any)(implicit currentMethod : String = Thread.currentThread.getStackTrace()(3).getMethodName) : Unit =
  {
    Predef.println(currentMethod + ": " + x)
  }

  def addMovement(p: PID, newPid: PID, movementRecorder: mutable.Map[(PID, PID), Int]) = {
    if (movementRecorder.contains(p, newPid)) {
      val currentCount = movementRecorder.get((p, newPid)).get
      movementRecorder.put((p, newPid), currentCount+1)
    } else {
      movementRecorder.put((p, newPid), 1)
    }
  }

  /* Calculates 'x' modulo 'mod', takes to consideration sign of x,
   * i.e. if 'x' is negative, than 'x' % 'mod' is negative too
   * so function return (x % mod) + mod in that case.
   */
  def nonNegativeMod(x: Int, mod: Int): Int = {
    val rawMod = x % mod
    rawMod + (if (rawMod < 0) mod else 0)
  }

  def printRepo() = {
    println("Repo: " + RDDRepo.repo)
    println("Roots: " + RDDRepo.getRoots())
    println(RDDRepo.traverse(RDDRepo.getRoots().head))
    println("Full graph: " + RDDRepo.graphAllTransforms)
    println("User graph: " + RDDRepo.graphUserTransforms)
  }

  def printAllStats(): Unit = {
    println(Console.BLUE + "ALL SHUFFLE STATS" + Console.BLACK)
    RDDRepo.shuffleStatsRepo.foreach((entry : ((RddTypes.RID, Map[String, Any]))) => {
      println("RID " + entry._1)
      println("NAME\t\t\t\t"+ Console.YELLOW_B + "BEFORE\t\t\t" + Console.GREEN_B + "AFTER\t\t\t"+Console.INVISIBLE)
      List("Size","MeanSize","StdDeviation","MaxPartitionSize").map((s : String) =>
        println(s+List.fill(if (s.length <= 4) 4 else if (s.length <= 8) 3 else if (s.length < 13) 2 else 1)("\t").mkString+Console.YELLOW_B+(math floor entry._2.apply("old"+s).asInstanceOf[Number].doubleValue()*100)/100+"\t\t\t"+Console.GREEN_B+(math floor entry._2.apply("new"+s).asInstanceOf[Number].doubleValue()*100)/100+"\t\t\t"+Console.INVISIBLE )
      )
//      entry._2.foreach(record : ((String, Any)))
    })
  }
}
