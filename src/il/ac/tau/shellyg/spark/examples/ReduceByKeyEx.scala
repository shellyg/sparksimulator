package il.ac.tau.shellyg.spark.examples

// scalastyle:off println
import il.ac.tau.shellyg.spark._

/** Computes an approximation to pi */
object ReduceByKeyEx {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Spark Pi")
    val sc = new SparkContext(conf)

    val a = sc.parallelize(List("dog", "cat", "owl", "gnu", "ant"), 3)
    val b = a.map(x => (x.length, x))
    println(b.collect())
    println("3 lettered: " + b.reduceByKey[Int, String](new HashPartitioner(2), _ + _).collect)
    // TODO: Get rid of the type parameters, if possible
    // Result: Array((3,dogcatowlgnuant))

    val c = sc.parallelize(List("dog", "tiger", "lion", "cat", "panther", "eagle"), 2)
    val d = c.map(x => (x.length, x))
    println("Mapped various number of letters: " + d.collect())
    println("Various number of letters: " + d.reduceByKey[Int, String](new HashPartitioner(2), _ + _).collect)
    // Result; Array((4,lion), (3,dogcat), (7,panther), (5,tigereagle))

    sc.stop()
  }
}

// scalastyle:on println
