package il.ac.tau.shellyg.spark

import il.ac.tau.shellyg.spark.RddTypes.RID
import scala.collection.mutable._

/**
  * Created by shelly on 20/02/16.
  */
class Graph() {

  val SHOULD_DEBUG = false

  val g : HashMap[RID, Buffer[(String, RID)]] = HashMap[RID, Buffer[(String, RID)]]()

  val roots : Buffer[RID] = Buffer[RID]()

  def add(r : RID, asRoot : Boolean = false) = {
    if (SHOULD_DEBUG) println("Adding RID" + r + " to the graph")
    g.put(r, Buffer[(String, RID)]())
    if (asRoot)
      roots += (r)
    //if (SHOULD_DEBUG) println("After adding node: G = " + g)
  }

  def addEdge(rp : RID, rc : RID, transform : String): Unit = {
    if (SHOULD_DEBUG) println("Adding an edge " + rp + " -(" + transform + ")-> " + rc)
    add(rc)
    g.get(rp).get += ((transform, rc))
    if (SHOULD_DEBUG) println("Graph with new edge = " + g)
  }

  // TODO: Add listen func on events of rp -> edge -> rc -> edge2 -> rc2
  def dfs(start : RID) : List[(String, RID)] = {

    def dfs0(v: (String, RID), visited: List[(String, RID)]) : List[(String, RID)] = {
      if (SHOULD_DEBUG) println("Visited = " + visited)
      if (visited.contains(v))
        visited
      else {

        val neighbors: List[(String, RID)] = g.get(v._2).get.filterNot(visited.contains).toList
        neighbors.foldLeft(v :: visited)((b, a) => dfs0(a, b))
      }
    }

    val recursResult = dfs0(("",start), List())
    if (SHOULD_DEBUG) println("Recursive result: " + recursResult)
    recursResult.reverse
  }


  def parent(p: RID) = {
    val filteredKeys = g.filterKeys((parent : RID) => {g.apply(parent).exists((x : (String, RID)) => x._2.equals(p))})
    if (SHOULD_DEBUG) println("Filtered parents for " + p + " are " + filteredKeys)
    if (filteredKeys.isEmpty)
      p
    else
      filteredKeys.head._1
  }

  override def toString = {
    val sb = new StringBuilder
    for (rootRid <- roots) {
      val traverse = dfs(rootRid)
      var lastNode = traverse.head._2
      for (edge <- traverse) {
        if (edge._2 == rootRid || parent(edge._2) == lastNode)
          sb.append("-").append(edge._1).append("->").append(edge._2)
        else
          sb.append(";").append(parent(edge._2)).append("-").append(edge._1).append("->").append(edge._2)

        lastNode = edge._2
      }
    }

    sb.toString()
  }
}
