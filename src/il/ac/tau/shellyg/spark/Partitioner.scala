package il.ac.tau.shellyg.spark

import il.ac.tau.shellyg.spark.RddTypes.PID

import scala.collection.mutable
import scala.reflect.ClassTag

/**
  * Created by shelly on 22/01/16.
  */
object DefaultPartitioner {
 /**
  * Choose a partitioner to use for a cogroup-like operation between a number of RDDs.
  *
  * If any of the RDDs already has a partitioner, choose that one.
  *
  * Otherwise, we use a default HashPartitioner. For the number of partitions, if
  * spark.default.parallelism is set, then we'll use the value from SparkContext
  * defaultParallelism, otherwise we'll use the max number of upstream partitions.
  *
  * Unless spark.default.parallelism is set, the number of partitions will be the
  * same as the number of partitions in the largest upstream RDD, as this should
  * be least likely to cause out-of-memory errors.
  *
  * We use two method parameters (rdd, others) to enforce callers passing at least 1 RDD.
  */
  def defaultPartitioner(rdd: PartitionedRDD[_, _], others: PartitionedRDD[_, _]*): Partitioner = {
    val bySize = (Seq(rdd) ++ others).sortBy(_.partitions.size).reverse
    for (r <- bySize if r.partitioner.isDefined && r.partitioner.get.numPartitions > 0) {
      return r.partitioner.get
    }

    new HashPartitioner(bySize.head.partitions.size)
  }
}

abstract class Partitioner() {
  def numPartitions: Int
  def getPartition(key: Any): RddTypes.PID
  override def equals(other: Any) : Boolean
}

class RandomPartitioner[K: ClassTag, V: ClassTag]
  (val partitions: Map[RddTypes.PID, mutable.Buffer[(K,V)]], val numPartitions: Int) extends Partitioner {
  override def getPartition(key: Any): RddTypes.PID = {
    // Returns just a random PID that contains the key
    for (elem <- partitions) {
      val buf = elem._2
      if (buf.toMap[K,V].keySet.contains(key.asInstanceOf[K])) {
        elem._1
      }
    }

    // If none found, return 0
    0
  }

  override def equals(other: Any): Boolean = other match {
    case r: RandomPartitioner[K,V] =>
      r.numPartitions == numPartitions
    case _ =>
      false
  }
}

/**
  * Copied from org.apache.spark.Partitioner - shellyg
  *
  * A org.apache.spark.Partitioner that implements hash-based partitioning using
  * Java's `Object.hashCode`.
  *
  * Java arrays have hashCodes that are based on the arrays' identities rather than their contents,
  * so attempting to partition an RDD[Array[_]] or RDD[(Array[_], _)] using a HashPartitioner will
  * produce an unexpected or incorrect result.
  */
class HashPartitioner(partitions: Int) extends Partitioner {
  require(partitions >= 0, s"Number of partitions ($partitions) cannot be negative.")

  def numPartitions: Int = partitions

  def getPartition(key: Any): RddTypes.PID = key match {
    case null => 0
    case _ => Utils.nonNegativeMod(key.hashCode, numPartitions)
  }

  override def equals(other: Any): Boolean = other match {
    case h: HashPartitioner =>
      h.numPartitions == numPartitions
    case _ =>
      false
  }

  override def hashCode: Int = numPartitions
}


