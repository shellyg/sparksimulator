package il.ac.tau.shellyg.spark.examples

// scalastyle:off println
import il.ac.tau.shellyg.spark._

/** Computes an approximation to pi */
object GroupByKeyEx {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Group By Key Example")
    val sc = new SparkContext(conf)

    val a = sc.parallelize(List("dog", "tiger", "lion", "cat", "spider", "eagle"), 3)
    val b = a.map(x => (x.length, x)) //
    val c = b.groupByKey(new HashPartitioner(2))
    println(c.partitions)
    println(c.glom.collect)
    // Result: Array[Array[(Int, Iterable[String])]] = Array(Array((4,CompactBuffer(lion)), (6,CompactBuffer(spider))), Array((3,CompactBuffer(dog, cat)), (5,CompactBuffer(tiger, eagle))))

    sc.stop()
  }
}

// scalastyle:on println
