package il.ac.tau.shellyg.spark.examples

// scalastyle:off println
import il.ac.tau.shellyg.spark._

/** Computes an approximation to pi */
object ParallelizeEx {
  // TODO: Write tests
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Spark Pi")
    val sc = new SparkContext(conf)
    val a = sc.parallelize(1 to 3, 3)
    val b = sc.parallelize(5 to 7, 2)

    val c = sc.parallelize(5 to 7, 7)

    // Check with foreachPartitions if was correctly parallelized.
    /* TODO: When parallelizing a range of size < numSlices, numSlices partitions are created, but some of them are empty
      * Actually the occupied partitions are not the first. Spark uses "slice" method
      */
    sc.stop()
  }
}

// scalastyle:on println
