package il.ac.tau.shellyg.spark.examples

// scalastyle:off println
import il.ac.tau.shellyg.spark._

/** Computes an approximation to pi */
object CartesianEx {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Spark Pi")
    val sc = new SparkContext(conf)
    val a = sc.parallelize(1 to 10, 3)
    val b = sc.parallelize(21 to 30, 7)

    val cart = a.cartesian(b)

    println(a.glom.collect)
    println(b.glom.collect)
    println(cart.collect())
    println(cart.glom.collect)

//    println(cart.partitions.values.toMap.asInstanceOf[Map[Int,Int]].keys)

    sc.stop()
  }
}

// scalastyle:on println
